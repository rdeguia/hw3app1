﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3app1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Raiders : ContentPage
	{
		public Raiders (string passData)
		{
			InitializeComponent ();

            passed.Text = passData;
		}
	}
}